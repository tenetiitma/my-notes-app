import {Component, OnInit, Renderer2} from '@angular/core';
import {Note} from '../../shared/note.model';
import {NotesService} from '../../shared/notes.service';

@Component({
  selector: 'app-notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.scss']
})
export class NotesListComponent implements OnInit {

  notes: Note[] = new Array<Note>();

  constructor(private notesService: NotesService) { }

  ngOnInit(): void {
    // retrieve all notes from NoteService
    this.notes = this.notesService.getAll();
  }

  deleteNote(id: number): void {
    this.notesService.delete(id);
  }

}
