import {Component, NgModule, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Note} from '../../shared/note.model';
import {NotesService} from '../../shared/notes.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-note-details',
  templateUrl: './note-details.component.html',
  styleUrls: ['./note-details.component.scss']
})

export class NoteDetailsComponent implements OnInit {

  note: Note;
  noteId: number;
  new: boolean;

  constructor(private notesService: NotesService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {

    // want to know if we add a new note or editing an existing one
    this.route.params.subscribe((params: Params) => {
      this.note = new Note();
      if (params.id) {
        this.note = this.notesService.get(params.id);
        this.noteId = params.Id;
        this.new = false;
      } else {
        this.new = true;
      }
    });
  }

  onSubmit(form: NgForm): void {
    if (this.new) {
      // we should save the note
      this.notesService.add(form.value);
    } else {
      this.notesService.update(this.noteId, form.value.title, form.value.body);
    }
    this.router.navigateByUrl('/');

  // .then (r => {
  //     if (r) {
  //       console.log('Navigation successful');
  //     } else {
  //       console.log('Navigation failed!');
  //     }
  //   });
  }

  cancel(): void {
    this.router.navigateByUrl('/');
  }
}
